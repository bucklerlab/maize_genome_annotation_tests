Figures that can be recomputed are stored here.

In Rmd - an html file with figures will be computed and stored locally.

Please do not commit large visualizations of the data.  Commit the code to create the visualizations.

