#Clone files to local directory from NCBI
library(curl)
url_base = "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/plant/Zea_mays/latest_assembly_versions/GCA_000005005.6_B73_RefGen_v4/"
genome_seq = "GCA_000005005.6_B73_RefGen_v4_genomic.fna.gz"
proteins = "GCA_000005005.6_B73_RefGen_v4_protein.faa.gz"
gff = "GCA_000005005.6_B73_RefGen_v4_genomic.gff.gz"
rna = "GCA_000005005.6_B73_RefGen_v4_rna_from_genomic.fna.gz"

download.file(paste0(url_base,genome_seq),paste0("local_data/",genome_seq))
download.file(paste0(url_base,proteins),paste0("local_data/",proteins))
download.file(paste0(url_base,gff),paste0("local_data/",gff))
download.file(paste0(url_base,rna),paste0("local_data/",rna))

