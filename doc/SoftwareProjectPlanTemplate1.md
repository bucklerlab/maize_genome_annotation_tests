
#Project Plan Name 
| Plan Specifics  |  Details |
|:---|---|
|**Epic**  |  Link to Jira epic |
|**Document status**   | DRAFT  |
|**Project Lead**   | Person who decides project is successfully completed  |
|**Partners** | List of biologists and programmers |
|**Software Lead** |  Main programmer(s) |

## Objective: Problem Statement

Write your problem statement, project objective here.

## Success metrics

Explain the metrics used to determine whether your objectives were successful.

|Goal| Metric  |
|---|---|
|  Goal 1 |  Metrics for goal 1 |
| Goal 2 | Metrics for goal 2   |
|   |   |

## Assumptions

###Biology assumptions:

* assumption 1
* assumption 2

###Software Assumptions:

* assumption 1
* assumption 2

## Tests (Biology and Software)
Specifc unit tests should be covered in jira tasks.  This table is intended to capture tests used to prove biological relevance.  They should be biologist approved

|   Test Description|Data Input | Expected Result   |
|---|---|---|
| Test1  | Data used for test| Expected output  |
| Test2  | Data used for test| Expected output |

## Milestones



## Requirements

| #  | Requirement  | Importance  | Jira Issue | Status |
|---:|---|---|---|---|
| 1 | Req 1  |  *high* | PHG-XXX  |  backlog/in progress/done |
|   |   |   |   |   |

## Requirements Details

Add details for each requirement here.  Use any format: bullet lists, paragraphs, etc.

## User Interactions


## Open Questions

This table contains a list of questions that need answering as the project develops.  It could be blank.

|  Question | Answer  | Source/Date Answered  |
|---|---|---|
|  If Pi is never ending, why is there world hunger? | Because some people prefer cake.  | Answered by Lynn, April 19, 2019  |
|   |   |   |


## Risks

List any risks associated with the project, e.g. dependencies on outside resources, quality of data, etc.
## Out of Scope

List the features discussed which are out of scope or might be revisited in a later release.